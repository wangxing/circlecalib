# CircleCalib

circlecalib use opencv
待完善

1.设置文件

setting xml
example:
  <!-- Number of inner corners per a item row and column. (square, circle) -->
  棋盘格行数，列数
  <BoardSize_Width>8</BoardSize_Width>
  <BoardSize_Height>7</BoardSize_Height>
  input 文件，需要的标定图片（优化，可增加直接利用视频进行标定）
  <!-- The input to use for calibration. 
   To use an image list   -> give the path to the XML or YAML file containing the list of the images, like "/tmp/circles_list.xml"
  -->
  <Input>"temp_input.xml"</Input>
  
  图片个数，设置则使用设置图片个数，如果设置个数比实际图片大，则使用实际图片个数进行标定
  <!-- How many frames to use, for calibration. -->
  <Calibrate_NrOfFrameToUse>23</Calibrate_NrOfFrameToUse>
  
  <!-- The size of a square in some user defined metric system (pixel, millimeter)-->
  棋盘格大小，单位mm
  <Square_Size>8</Square_Size>
  <!-- The name of the output log file. -->
  输出文件名
  <Write_outputFileName>"caliberation_result.txt"</Write_outputFileName>

 需要信息，棋盘格行数列数，图片列表，棋盘格大小。（后面这个会提出yaml）
 输出：相机的内参矩阵。 opencv里的说内参数是4个其为fx、fy、u0、v0。实，u0，v0表示图像的中心像素坐标和图像原点像素坐标之间相差的横向和纵向像素数。
 相机的外参数是6个：三个轴的旋转参数分别为（ω、δ、 θ，旋转向量，T的三个轴的平移参数（Tx、Ty、Tz）。
 2.标定过程
    读入设置文件及标定图片。
	在每一副图片中，找到角点信息，并进行亚像素化，得到更精确的坐标值。
	findChessboardCorners(imageInput, board_size, image_points_buf)；
	cornerSubPix(view_gray, image_points_buf, Size(5, 5), Size(-1, -1), TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.001));
	在这里可以显示角点坐标信息。
	drawChessboardCorners(view_gray, board_size, image_points_buf, false);
	
	计算得到棋盘点在三维世界坐标系的坐标，这里可以认为其z=0。
	进行标定，利用棋盘角点世界坐标系的位置，图像中的位置。计算出内外参矩阵。
	calibrateCamera(object_points, image_points_seq, image_size, cameraMatrix, distCoeffs, rvecsMat, tvecsMat, 0);
	
	cameraMatrix：内参矩阵。
	distCoeffs：畸变矩阵。
	rvecsMat：旋转矩阵。
	tvecsMat：平移矩阵。
	
3.评价及校正过程
     对世界坐标系下的棋盘角点位置信息，利用标定结果矩阵，进行反向投影，与已知角点在图像中的位置进行比对，进行误差分析。
	 projectPoints(tempPointSet, rvecsMat[i], tvecsMat[i], cameraMatrix, distCoeffs, image_points2);
	 我们可以得到每幅图的误差以及所有点的误差。
	 err = norm(image_points2Mat, tempImagePointMat, NORM_L2);
	 
	 利用标定结果可以对图片进行校正。
	 initUndistortRectifyMap(cameraMatrix, distCoeffs,
			                    R, cameraMatrix,
			                    image_size, CV_32FC1, mapx, mapy);
	 remap(imageSource, newimage, mapx, mapy, INTER_LINEAR);
	 
	 1219.bmp是校正前图片，1219_d.jpg是校正后图片。
	 
	 
4.具体原理参见另外文档。
	 
	
 