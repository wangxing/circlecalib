#include <iostream>
#include "opencv2/core/core.hpp" 
#include "calib/settings.h"
#include "calib/calibration.h"

static void help()
{
    std::cout <<  "This is a camera calibration sample." << endl;
    std::cout <<  "Usage: calibration configurationFile"  << endl;
    std::cout <<  "Near the sample file you'll find the configuration file, which has detailed help of ";
    std::cout <<  "how to edit it.  It may be any OpenCV supported file format XML/YAML." << endl;
}

static void read(const FileNode& node, Settings& x, const Settings& default_value = Settings())
{
    if (node.empty())
    {
        x = default_value;
    }
        
    else
    {
        x.read(node);
    }     
}

int main()
{
    help();
    Settings settings;
    const string inputSettingsFile = "temp_data.xml";

    FileStorage fs(inputSettingsFile, FileStorage::READ); 
    if (!fs.isOpened())
    {
        std::cout << "Could not open the configuration file: \"" << inputSettingsFile << "\"" << std::endl;
	return -1;
    }
    fs["Settings"] >> settings;
    fs.release();  

    if (!settings.goodInput)
    {
        std::cout << "Invalid input detected. Application stopping. " << std::endl;
	return -1;
    }
    
    Calibration calibration;
    calibration.calibProcessor(&settings);
    return 0;
}
