#include "settings.h"

//Write serialization for this class
void Settings::write(FileStorage& fs) const
{
	fs << "{" << "BoardSize_Width" << boardSize.width
	   << "BoardSize_Height" << boardSize.height
	   << "SquareSize_Width" << squareSize.width
	   << "SquareSize_Height" << squareSize.height
	   << "ImageSize_Width" << imageSize.width
	   << "ImageSize_Height" << imageSize.height
	   << "Write_outputFileName" << outputFileName
	   << "Write_IntrinsicsFile" << IntrinsicsFileName
	   << "Calibrate_ImageNumber" << numberImage
	   << "Input" << input
	   << "InputEx" << inputEx
	   << "mode" << mode
	   << "}";
}

//Read serialization for this class
void Settings::read(const FileNode& node)                          
{
	node["BoardSize_Width"] >> boardSize.width;
	node["BoardSize_Height"] >> boardSize.height;
	node["SquareSize_Width"] >> squareSize.width;
	node["SquareSize_Height"] >> squareSize.height;
	node["ImageSize_Width"] >> imageSize.width;
	node["ImageSize_Height"] >> imageSize.height;
	node["Calibrate_ImageNumber"] >> numberImage;
	node["Write_outputFileName"] >> outputFileName;
	node["Write_IntrinsicsFile"] >> IntrinsicsFileName;
	node["Input"] >> input;
	node["InputEx"] >> inputEx;
	node["mode"] >> mode;
	interprate();
}

void Settings::interprate()
{
	goodInput = true;
	if (boardSize.width <= 0 || boardSize.height <= 0)
	{
		cerr << "Invalid Board size: " << boardSize.width << " " << boardSize.height << endl;
		goodInput = false;
	}
	if (squareSize.width <= 0 || squareSize.height <= 0)
	{
		cerr << "Invalid square size " << squareSize << endl;
		goodInput = false;
	}

	if (numberImage <= 0)
	{
		cerr << "Invalid number of frames " << numberImage << endl;
		goodInput = false;
	}

    if (0 == mode)     // Check for valid input
	{
	    if(input.empty())
		{
		    inputType = INVALID;
		}
		else
		{
		    if (readStringList(input, imageList))
		    {
			    inputType = IMAGE_LIST;
			    numberImage = (numberImage < (int)imageList.size()) ? numberImage : (int)imageList.size();
     	    }	
		}	
	}
	else if(1 == mode)
	{
		if(input.empty() || inputEx.empty())
		{
			inputType = INVALID;
		}
		else
		{
			if (readStringList(input, imageList))
		    { 
			    numberImage = (numberImage < (int)imageList.size()) ? numberImage : (int)imageList.size();
				int numberImageEx = 0;
				if(readStringList(inputEx, imageExList))
				{
				    numberImageEx = (numberImage < (int)imageExList.size()) ? numberImage : (int)imageExList.size();	
					if(numberImage != numberImageEx)
					{
					    inputType = INVALID;	
					}
					else
					{
						inputType = IMAGE_LIST;
					}					
				}
				else
				{
					inputType = INVALID;
				}
				
     	    }	
		}
	}

	if (inputType == INVALID)
	{
		cerr << " Inexistent input: " << input;
		goodInput = false;
	}
}

bool Settings::readStringList(const string& filename, vector<string>& l)
{
	l.clear();
	FileStorage fs(filename, FileStorage::READ);
	if (!fs.isOpened())
	{
		return false;
	}
		
	FileNode n = fs.getFirstTopLevelNode();
	if (n.type() != FileNode::SEQ)
	{
		return false;
	}	
	FileNodeIterator it = n.begin(), it_end = n.end();
	for (; it != it_end; ++it)
	{
		l.push_back((string)*it);
	}		
	return true;
}