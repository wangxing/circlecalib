#include "calibration.h"
Calibration::Calibration()
{

}

int Calibration::calibProcessor(Settings *settings)
{   
    m_board_size = settings->boardSize;
    m_square_size = settings->squareSize;
    m_image_size = settings->imageSize;
    cout << "Start extracting corners..."<<endl;

    m_image_count = findCornerPoints(settings);
    if (-1 == m_image_count)
    {
        return -1;
    }
    
    outputCornetPointsResult();
    cout << "Corner extraction completed!"<<endl;
    cout << "Start calibration...";
    m_intrinsic = Mat(3, 3, CV_32FC1, Scalar::all(0));   
    //5 distortion coefficients of the camera k1,k2,p1,p2
    m_distortion_coeff = Mat(1, 5, CV_32FC1, Scalar::all(0)); 

    getObjectPoints();
    calibrateCamera(m_object_points_vec, m_all_detect_corners_vec, m_image_size, m_intrinsic, m_distortion_coeff, m_rvecs, m_tvecs, 0);
    cout << "Calibration completed"<<endl; 
    
    outputIntrinsicParam(settings->IntrinsicsFileName);
    evaluationCalibResult(settings->outputFileName);
    string file_old_Path = "rc/1219.bmp";
    string file_new_path  = "rc/1219_d.png";
    displayCalibResult(file_old_Path, file_new_path);
   
    waitKey(0);
    return 0;
}

void Calibration::outputCornetPointsResult()
{
    int total = m_all_detect_corners_vec.size();
    cout << "total = " << total << endl; 
    for (int ii = 0; ii < total; ii++)
    {
        cout << "--> NO. " << ii + 1 << " image data -->:";
        for(int jj = 0; jj < m_all_detect_corners_vec[ii].size(); jj++)
        {
            if (0 == jj % 3)	
            {
                cout << endl;
            }
            else
            {
                cout.width(10);
            }
            cout << " -->" << m_all_detect_corners_vec[ii][jj].x;
            cout << " -->" << m_all_detect_corners_vec[ii][jj].y;
        }
        cout<<endl;
    }
}

int Calibration::findCornerPoints(Settings *settings)
{ 
    int image_count = 0;
    for(int i = 0; i < settings->numberImage; i++)
    {
        vector<Point2f> each_detect_corners_vec;  
	    image_count++;		

	    cout << "image_count = " << image_count << endl;
	    Mat imageInput = imread(settings->imageList[i]);
	   
        //Get image width and height information when reading the first image
	    if (1 == image_count) 
	    {
            m_image_size.width = imageInput.cols;
	        m_image_size.height = imageInput.rows;
	        cout << "image_size.width = " << m_image_size.width << endl;
	        cout << "image_size.height = " << m_image_size.height << endl;
     	}

	    if (0 == findChessboardCorners(imageInput, m_board_size, each_detect_corners_vec))
	    {
	        cout << "can not find chessboard corners!" << endl;
	        return -1;
    	}
    	else
    	{
	        Mat view_gray;
	        cvtColor(imageInput, view_gray, CV_RGB2GRAY);
	   
            // Sub pixel precision
	        cornerSubPix(view_gray, 
                         each_detect_corners_vec,
                         Size(5, 5),
                         Size(-1, -1), 
                         TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER,
                         30,
                         0.001));
	        //method 2
	        //find4QuadCornerSubpix(view_gray, each_detect_corners_vec, Size(5, 5)); 
	        m_all_detect_corners_vec.push_back(each_detect_corners_vec);
	        drawChessboardCorners(view_gray, m_board_size, each_detect_corners_vec, false);
	        namedWindow("Camera Calibration", WINDOW_NORMAL);
	        imshow("Camera Calibration", view_gray);
	        waitKey(5);
	    }
    }
    return image_count;
}

void Calibration::getObjectPoints()
{	
    vector<Point3f> temp_point_vec;
    for (int row_index = 0; row_index < m_board_size.height; row_index++)
	{
	    for (int col_index = 0; col_index < m_board_size.width; col_index++)
	    {
	        Point3f real_point_temp;
		    real_point_temp.x = row_index * m_square_size.width;
		    real_point_temp.y = col_index * m_square_size.height;
		    real_point_temp.z = 0;
		    temp_point_vec.push_back(real_point_temp);
	    }
	}	

    for (int image_index = 0; image_index < m_image_count; image_index++)
    {
        
	    m_object_points_vec.push_back(temp_point_vec);
    }    
}

void Calibration::evaluationCalibResult(string out_file_name)
{
    ofstream fout(out_file_name);
    cout << "Start evaluating calibration results..."<<endl;
    double total_error = 0.0;
    double error = 0.0; 
    double point_counts_each = m_board_size.height * m_board_size.width;
    //Save the recalculated projection point
    vector<Point2f> image_points_project_vec;
    cout << "Calibration error for each image"<<endl;
    fout << "Calibration error for each image"<<endl;
   
    for (int i = 0; i < m_image_count; i++)
    {
        vector<Point3f> temp_object_point_vec = m_object_points_vec[i];
	    // Through the obtained internal and external parameters of the camera,
	    //the 3D points of the space are re-projected to obtain a new projection point.
	    projectPoints(temp_object_point_vec, m_rvecs[i], m_tvecs[i], m_intrinsic, m_distortion_coeff, image_points_project_vec);
        //Calculate the error between the new projection point and the old projection point
	    vector<Point2f> temp_image_points_vec = m_all_detect_corners_vec[i];
	    Mat tempImagePointMat = Mat(1, temp_image_points_vec.size(), CV_32FC2);
	    Mat image_points2Mat = Mat(1, image_points_project_vec.size(), CV_32FC2);
     	for (int j = 0; j < temp_image_points_vec.size(); j++)
	    {
            image_points2Mat.at<Vec2f>(0, j) = Vec2f(image_points_project_vec[j].x, image_points_project_vec[j].y);
            tempImagePointMat.at<Vec2f>(0, j) = Vec2f(temp_image_points_vec[j].x, temp_image_points_vec[j].y);
     	}
     	error = norm(image_points2Mat, tempImagePointMat, NORM_L2);
	    total_error += error /= point_counts_each;
	    std::cout << "No." << i + 1 << " Average error of the image: " << error << " pixel" << endl;	    
        fout << "No." << i + 1 << " Average error of the image:" << error << " pixel" << endl;
    }
    std::cout << "Total error all the imag:" << total_error / m_image_count << " pixel" << endl;
    fout << "Total error all the image:" << total_error / m_image_count << " pixel" << endl;
    std::cout << "Calibration completed!" << endl;
    //Save calibration results      
    std::cout << "Start saving calibration results..." << endl;
    Mat rotation_matrix = Mat(3, 3, CV_32FC1, Scalar::all(0)); 
    fout << "In parameter matrix" << endl;
    fout << m_intrinsic << endl;
    fout << "Distortion coefficient"<<endl;
    fout << m_distortion_coeff << endl;
    for (int i = 0; i < m_image_count; i++)
    {
	fout << "No." << i + 1 << ":Image translation vector: " << endl;
	fout << m_tvecs[i] << endl;
	//Convert the rotation vector to the corresponding rotation matrix
	Rodrigues(m_tvecs[i], rotation_matrix);
	fout << "No." << i + 1 << "Image rotation matrix: " << endl;
	fout << rotation_matrix << endl;
	fout << "No." << i + 1 << " Image rotation vector: " << endl;
	fout << m_rvecs[i] << endl << endl;
    }
    std::cout << "Finish saving" << endl;
    fout << endl;
    fout.close();
}

void Calibration::displayCalibResult(string file_old_Path,string file_new_path)
{
    Mat mapx = Mat(m_image_size, CV_32FC1);
    Mat mapy = Mat(m_image_size, CV_32FC1);
    Mat R = Mat::eye(3, 3, CV_32F);
    cout << "Save corrected image" << endl;

	//Used to calculate the distortion map
	initUndistortRectifyMap(m_intrinsic, 
                            m_distortion_coeff,
	                        R,                               
                            m_intrinsic,
	                        m_image_size,
                            CV_32FC1,
                            mapx, 
                            mapy);

	Mat image_old = imread(file_old_Path);
	Mat image_new = image_old.clone();
	// Apply the obtained mapping to the image
	remap(image_old, image_new, mapx, mapy, INTER_LINEAR);
	//another method
	//undistort(image_old,image_new,m_intrinsic, m_distortion_coeff);														   
	
	//Save the corrected image
	imwrite(file_new_path, image_new);
	imshow("Original Image", image_old);
	waitKey(500);
	imshow("Undistorted Image", image_new);
	waitKey(500);
    destroyWindow("Original Image");
	destroyWindow("Undistorted Image");
}

void Calibration::setImageSize(Size image_size)
{
     m_image_size = image_size;
}

void Calibration::outputIntrinsicParam(string intrinsics_file_name)
{
	cout << "fx :" << m_intrinsic.at<double>(0, 0) << endl << "fy :" << m_intrinsic.at<double>(1, 1) << endl;
	cout << "cx :" << m_intrinsic.at<double>(0, 2) << endl << "cy :" << m_intrinsic.at<double>(1, 2) << endl;
	cout << "k1 :" << m_distortion_coeff.at<double>(0, 0) << endl;
	cout << "k2 :" << m_distortion_coeff.at<double>(1, 0) << endl;
	cout << "p1 :" << m_distortion_coeff.at<double>(2, 0) << endl;
	cout << "p2 :" << m_distortion_coeff.at<double>(3, 0) << endl;
	cout << "p3 :" << m_distortion_coeff.at<double>(4, 0) << endl;
	
    FileStorage fs(intrinsics_file_name, FileStorage::WRITE);
	if (fs.isOpened())
	{   
		fs << "cameraMatrix" << m_intrinsic << "distCoeff" << m_distortion_coeff;
		fs.release();
		cout << "Matrix Right write successful!" << endl;
	}
	else
    {
        cout << "Error: can not save the extrinsic parameters?"<<endl;
    }		
}