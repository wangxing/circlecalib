#pragma once
#include "opencv2/core/core.hpp"  
#include "opencv2/imgproc/imgproc.hpp"  
#include "opencv2/calib3d/calib3d.hpp"  
#include "opencv2/highgui/highgui.hpp"  
#include <iostream>  
#include <fstream>
#include "settings.h"

using namespace cv;
using namespace std;

class Calibration
{
public:
    Calibration();
public:
    int calibProcessor(Settings *settings);
    void displayCalibResult(string file_old_path, string file_new_path);
    void evaluationCalibResult(string out_file_name);
    void setImageSize(Size image_size);
private:
    int findCornerPoints(Settings *settings);
    void getObjectPoints();
    void outputCornetPointsResult();
    void outputIntrinsicParam(string intrinsics_file_name);
    
private:
   int  m_image_count;
   Size m_image_size;
   Size m_board_size;
   Size m_square_size;
   vector<vector<Point2f>> m_all_detect_corners_vec;
   vector<vector<Point3f>> m_object_points_vec;
   Mat m_intrinsic;
   Mat m_distortion_coeff;
   vector<Mat> m_tvecs;
   vector<Mat> m_rvecs;
};
