#pragma once
#include "opencv2/core/core.hpp"    
#include <iostream>  
#include <fstream>

using namespace std;
using namespace cv;

enum InputType
{
	INVALID,
	IMAGE_LIST
};

class Settings
{
public:
	Settings() : goodInput(false) {}
public:
	void read(const FileNode& node);
    void write(FileStorage& fs) const;	
	void interprate();
	bool readStringList(const string& filename, vector<string>& l);

public:
	Size boardSize;              // The size of the board -> Number of items by width and height
	Size squareSize;             // The size of a square in your defined unit (point, millimeter,etc).
	Size imageSize;             // The size of image to use.
	string outputFileName;       // The name of the file where to write
	string IntrinsicsFileName;       // The name of the file where to write
	 
	int mode;                    //mode 0 singe, 1 double
	bool goodInput;
	int numberImage;                // The number of frames to use from the input for calibration
	string input;
	string inputEx;                // The input ->

    vector<string> imageList;
	vector<string> imageExList;
	InputType inputType;
};
