#include <iostream>
#include <opencv2/opencv.hpp>
#include <math.h>

using namespace std;
using namespace cv;

int main()
{
    VideoCapture cap_left(0);
    VideoCapture cap_right(1);
    if(!cap_left.isOpened())
    {
        cout<<"could not open camera0..."<<endl;
        return -1;
    }

    if(!cap_right.isOpened())
    {
        cout<<"could not open camera1..."<<endl;
        return -2;
    }
    //cap_left.set(CV_CAP_PROP_FRAME_WIDTH, 2560);
    //cap_left.set(CV_CAP_PROP_FRAME_HEIGHT, 1440)

    int i = 0;
    cv::Mat img_left;
    cv::Mat img_right;
    char filename_left[50];
    char filename_right[50];
    while (cap_left.read(img_left) && cap_right.read(img_right))
    {
        imshow("img_left", img_left);
        imshow("img_right", img_right);

        char c = cv::waitKey(5);
        if (c == 'a' || c == 'A')
        {
           cout<<"aaaaaaaaaaaaaaaaaaaaaaaaa"<<endl;
           sprintf(filename_left, "left/left%d.jpg", i);
           imwrite(filename_left, img_left);
           sprintf(filename_right, "right/right%d.jpg", i);
           imwrite(filename_right, img_right);
           i++;
        }

        if (c == 'q' || c == 'Q')
        {
            break;
        }

    }
    cap_left.release();
    cap_right.release();
    return 0;
}
